<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('home',[App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('about',[App\Http\Controllers\AboutController::class, 'index'])->name('about');
Route::get('portofolio',[App\Http\Controllers\PortofolioController::class, 'index'])->name('portofolio');
Route::get('blog',[App\Http\Controllers\BlogController::class, 'index'])->name('blog');
Route::get('contact',[App\Http\Controllers\ContactController::class, 'index'])->name('contact');




// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/home', function () {
//     $page=1;
//     return view('home', compact('page'),["title"=>"Dwita"]);
// })->name('home');

// Route::get('/about', function () {
//     $page=2;
//     return view('about', compact('page'),["title"=>"About"]);
// })->name('about');

// Route::get('/portofolio', function () {
//     $page=3;
//     return view('portofolio', compact('page'),["title"=>"Portofolio"]);
// })->name('portofolio');

// Route::get('/blog', function () {
//     $page=4;
//     return view('blog', compact('page'),["title"=>"Blog"]);
// })->name('blog');

// Route::get('/contact', function () {
//     $page=5;
//     return view('contact', compact('page'),["title"=>"Contact"]);
// })->name('contact');

